package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Photo extends Application {
    ArrayList<String> albums;
    ArrayList<String> thumbnails;
    File f;

    @Override
    public void start(Stage primaryStage) throws Exception{
        albums = new ArrayList<String>();
        thumbnails = new ArrayList<String>();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Image Album");
        readFile();
        mainMenu(primaryStage);
    }

    /**
     * Show Main menu and add different UI elements for the first main menu
     */
    private void mainMenu(Stage primaryStage) {
        Label label = new Label("Welcome to the Image Album");
        Button button1 = new Button("See Albums");
        Button button2 = new Button("Add new Album");

        button1.setOnAction(e -> {

            ShowAlbums(primaryStage);
        });
        button2.setOnAction(value ->  {
            Label l = new Label("Enter Album Name:");
            TextField textField = new TextField();
            Button browse = new Button("Thumbnail");
            browse.setOnAction(e ->  {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
                f = fileChooser.showOpenDialog(new Stage());
                if(f != null) {
                    try {
                        thumbnails.add(f.toURI().toURL().toExternalForm());
                    } catch (MalformedURLException malformedURLException) {
                        malformedURLException.printStackTrace();
                    }
                }
            });
            Button accept = new Button("Accept");
            accept.setOnAction(e ->  {
                if(f == null || textField.getText() == "")
                    return;
                try {
                    albums.add(textField.getText());
                    thumbnails.add(f.toURI().toURL().toExternalForm());
                    writeFile();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                mainMenu(primaryStage);
            });
            VBox vbox = new VBox(l,textField,browse,accept);
            vbox.setAlignment(Pos.CENTER);
            vbox.setSpacing(10);
            Button cancel = new Button("Cancel");
            cancel.setOnAction(e1 ->  {
                mainMenu(primaryStage);
            });
            vbox.getChildren().add(cancel);
            primaryStage.setScene(new Scene(vbox, 300, 275));
        });

        button1.setAlignment(Pos.CENTER);
        button2.setAlignment(Pos.CENTER);

        VBox vbox = new VBox(label,button1,button2);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);

        primaryStage.setScene(new Scene(vbox, 300, 275));
        primaryStage.show();
    }

    /**
     * Show List of all albums and put button for doing interaction for each album
     * At the first lines of this method we defined a search box and search button
     * to implement the search feature in the albums.
     */
    private void ShowAlbums(Stage primaryStage) {
        // Search
        TextField searchText = new TextField();
        Button search = new Button("Search");
        search.setOnAction(e1 -> {
            searchMethod(primaryStage, searchText);
        });
        //
        VBox albumList = new VBox(searchText,search);
        for (int i = 0; i < albums.size(); i++) {
            Button b = new Button(albums.get(i));
            b.setOnAction(e1 -> {
                try {
                    ShowAlbum(primaryStage,b.getText());
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

            });
            System.out.println("thumbnails.get(i) "+thumbnails.get(i));
            ImageView iv = new ImageView(thumbnails.get(i));
            iv.setFitHeight(100);
            iv.setFitWidth(100);
            GridPane pane = new GridPane();

            pane.getChildren().add(iv);

            Button delete = new Button("delete"+i);
            delete.setOnAction(e1 -> {
                int ii = Integer.parseInt(delete.getText().substring(6));
                albums.remove(ii);
                thumbnails.remove(ii);
                try {
                    writeFile();
                    ShowAlbums(primaryStage);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });

            HBox hbox = new HBox(b,delete);
            pane.getChildren().add(hbox);

            pane.setAlignment(Pos.CENTER);

            albumList.getChildren().add(pane);
        }
        albumList.setAlignment(Pos.CENTER);
        albumList.setSpacing(10);
        Button cancel = new Button("Back");
        cancel.setOnAction(e1 ->  {
            mainMenu(primaryStage);
        });
        albumList.getChildren().add(cancel);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(albumList);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setFitToWidth(true);
        primaryStage.setScene(new Scene(scrollPane, 300, 275));
    }

    /**
     * By clicking on the search button in the albums part we call this function
     * In this function we search in the name of the albums and show the albums
     * which has entered text in their name
     */
    private void searchMethod(Stage primaryStage, TextField searchText) {
        // Search
        Button search = new Button("Search");
        search.setOnAction(e1 -> {
            searchMethod(primaryStage, searchText);
        });
        //
        VBox albumList = new VBox(searchText,search);
        for (int i = 0; i < albums.size(); i++) {
            if(albums.get(i).contains(searchText.getText()))
            {
                Button b = new Button(albums.get(i));
                b.setOnAction(e2 -> {
                    try {
                        ShowAlbum(primaryStage,b.getText());
                    } catch (FileNotFoundException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }

                });
                System.out.println("thumbnails.get(i) "+thumbnails.get(i));
                ImageView iv = new ImageView(thumbnails.get(i));
                iv.setFitHeight(100);
                iv.setFitWidth(100);
                GridPane pane = new GridPane();

                pane.getChildren().add(iv);

                Button delete = new Button("delete"+i);
                delete.setOnAction(e2 -> {
                    int ii = Integer.parseInt(delete.getText().substring(6));
                    albums.remove(ii);
                    thumbnails.remove(ii);
                    try {
                        writeFile();
                        ShowAlbums(primaryStage);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });

                HBox hbox = new HBox(b,delete);
                pane.getChildren().add(hbox);

                pane.setAlignment(Pos.CENTER);

                albumList.getChildren().add(pane);
            }
        }
        albumList.setAlignment(Pos.CENTER);
        albumList.setSpacing(10);
        Button cancel = new Button("Back");
        cancel.setOnAction(e2 ->  {
            mainMenu(primaryStage);
        });
        albumList.getChildren().add(cancel);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(albumList);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setFitToWidth(true);
        primaryStage.setScene(new Scene(scrollPane, 300, 275));
    }

    /**
     * We show the details of one album in this method. In this method we show the images
     * and put button for back to main menu and deleting images or doing some other works
     */
    private void ShowAlbum(Stage primaryStage,String name) throws IOException {
        TextField tf = new TextField();
        Button rename = new Button("Rename");
        rename.setOnAction(e ->  {
            for (int i = 0; i < albums.size(); i++) {
                if(albums.get(i).equals(name)) {
                    albums.remove(i);
                    albums.add(i,tf.getText());
                    try {
                        writeFile();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        });
        Button b = new Button("Add image");
        b.setOnAction(e ->  {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose image");
            f = fileChooser.showOpenDialog(new Stage());
            if(f == null)
                return;
            FileWriter writer = null;
            try {
                writer = new FileWriter("src/sample/"+name+".txt",true);
                writer.write(f.toURI().toURL().toExternalForm() + System.lineSeparator());
                writer.close();
                ShowAlbum(primaryStage,name);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        VBox vbox = new VBox(tf,rename,b);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);

        File file = new File("src/sample/"+name+".txt");
        if(!file.exists())
            file.createNewFile();

        Scanner scanner = new Scanner( new File("src/sample/"+name+".txt"), "UTF-8" );
        if(scanner.hasNext()) {
            String text = scanner.useDelimiter("\\A").next();
            ArrayList<String> images = new ArrayList<String>(Arrays.asList(text.split(System.lineSeparator())));
            scanner.close(); // Put this call in a finally block
            for (int i = 0; i < images.size(); i++) {
                ImageView iv = new ImageView(images.get(i));
                iv.setFitHeight(100);
                iv.setFitWidth(100);

                Button delete = new Button("Delete"+i);
                delete.setOnAction(e ->  {
                    int ii = Integer.parseInt(delete.getText().substring(6));
                    images.remove(ii);

                    FileWriter writer = null;
                    try {
                        writer = new FileWriter("src/sample/"+name+".txt");
                        for (int j = 0; j < images.size(); j++) {
                            writer.write(images.get(j) + System.lineSeparator());
                        }
                        writer.close();
                        ShowAlbum(primaryStage,name);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
                GridPane pane = new GridPane();

                pane.getChildren().add(iv);
                pane.getChildren().add(delete);

                pane.setAlignment(Pos.CENTER);

                vbox.getChildren().add(pane);

            }
        }
        Button cancel = new Button("Back");
        cancel.setOnAction(e ->  {
            mainMenu(primaryStage);
        });
        vbox.getChildren().add(cancel);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vbox);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setFitToWidth(true);
        primaryStage.setScene(new Scene(scrollPane, 300, 275));
    }

    /**
     * We write two main array list of our project to the file
     * In this method we write Thumbnails and name of albums inside the file
     */
    private void writeFile() throws IOException {
        FileWriter albumWriter = new FileWriter("src/sample/albums.txt");
        for(String str: albums) {
            albumWriter.write(str + System.lineSeparator());
        }
        albumWriter.close();

        FileWriter thumbnailWriter = new FileWriter("src/sample/thumbnails.txt");
        for(String str: thumbnails) {
            thumbnailWriter.write(str + System.lineSeparator());
        }
        thumbnailWriter.close();
    }

    /**
     * In this method we read Thumbnails list and album name lists from the file
     * and initialize our arraylists.
     */
    private void readFile() throws IOException {
        Scanner scanner = new Scanner( new File("src/sample/albums.txt"), "UTF-8" );
        if(!scanner.hasNext())
            return;

        String text = scanner.useDelimiter("\\A").next();
        albums = new ArrayList<String>(Arrays.asList(text.split(System.lineSeparator())));
        scanner.close(); // Put this call in a finally block

        scanner = new Scanner( new File("src/sample/thumbnails.txt"), "UTF-8" );
        text = scanner.useDelimiter("\\A").next();
        thumbnails = new ArrayList<String>(Arrays.asList(text.split(System.lineSeparator())));
        scanner.close(); //
    }

    public static void main(String[] args) {
        launch(args);
    }
}
