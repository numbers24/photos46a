package objects;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Scanner;

/**album object
 *
 */
public class Album {

    String name;
    String thumbnail;
    ArrayList<Photo> album;

    /**album constructor
     *
     * @param name
     * @param thumbnail
     */
    public Album (String name, String thumbnail){
        this.name = name;
        this.thumbnail = thumbnail;
        this.album = new ArrayList<Photo>();
        addThumbnail();
    }

    /**adds thumbnail to the album
     *
     */
    public void addThumbnail(){
        try{
        Path p = Paths.get(thumbnail);
        File f = p.toFile();
        SimpleDateFormat sdf = new SimpleDateFormat("E, MMMM dd HH:mm:ss z yyyy");
        album.add(new Photo(thumbnail,sdf.format(f.lastModified()), "thumbnail", new ArrayList<Tag>()));
        } catch (Exception e) {System.out.println("err: could not add thumbnail to album");}
    }
    /**get the name of the album
     *
     * @return
     */
     public String getName(){return name;}
    /**get the thumbnail of the album
     *
     * @return
     */
    public String getThumbnail(){return thumbnail;}

    /** get the photos of the album
     *
     * @return
     */
    public ArrayList<Photo> getAlbum(){return album;}

    /**renames album
     *
     * @param name
     */
    public void setName(String name){this.name = name;}

    /**resets album photos
     *
     * @param album
     */
    public void setAlbum(ArrayList<Photo> album){this.album = album;}
    /**writes the album into a txt file
     *
     * @param user
     * @throws IOException
     */
    public void writeAlbum(String user) throws IOException {
        FileWriter w = new FileWriter("userdata/"+user+"/"+name+".txt");
        for(Photo p : album){

            w.write(p.imageDir + "\n" + p.date + "\n<caption>\n" + p.description + "\n</caption>\n");

            for(Tag t : p.Tags) {
                w.write(t.name);
                for (String s : t.value)
                    w.write(","+s);
                w.write("\n");
            }
            w.write("\n");
        }
        w.close();
    }

    /**reads the album txt file
     *
     * @param user
     * @throws IOException
     */
    public void readAlbum(String user) throws  IOException {
        album = new ArrayList<Photo>();
        Scanner s = new Scanner(new FileReader("userdata/"+user+"/"+name+".txt"));
        String line;
        while(s.hasNextLine()){
            String imageDir = s.nextLine();
            if(imageDir.equals(null)) break;
            String date = s.nextLine();
            String description="";

            if(s.nextLine().equals("<caption>")){
                String prev = s.nextLine();
                while (s.hasNextLine()){
                    line = s.nextLine();
                    if(line.equals("</caption>")){
                        description+= prev; break;
                    }
                    description += prev+"\n";
                    prev=line;
                }
            }
            ArrayList<Tag> Tags = new ArrayList<Tag>();
            while (s.hasNextLine()&&!(line = s.nextLine()).equals("")){
                String[] t = line.split(",");
                Tags.add(new Tag(t[0], new ArrayList<String>(Arrays.asList(Arrays.copyOfRange(t,1,t.length)))));
            }
            addPhoto(new Photo(imageDir,date,description,Tags));
        }
        s.close();
    }

    /**add a photo to the album
     *
     * @param p
     */
    public boolean addPhoto(Photo p){
        if(album.contains(p)) return false;
        else{album.add(p); return true;}
    }
}
