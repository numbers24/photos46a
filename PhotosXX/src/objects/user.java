package objects;

/** object for users
 *
 */
public class user {
    String name;
    String password;

    /**user constructor
     *
     * @param name
     * @param password
     */
    public user(String name,String password){
        this.name = name;
        this.password = password;
    }

    /**gets name of user
     *
     * @return
     */
    public String getName(){return name;}

    /**gets password of user
     *
     * @return
     */
    public String getPassword(){return password;}

}
