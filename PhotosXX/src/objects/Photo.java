package objects;
import java.util.ArrayList;

/**object for photo
 *
 */
public class Photo {

    //metadata of the photo
    String description;
    String imageDir;
    String date;
    ArrayList<Tag> Tags;

    /**constructor for photo
     *
     * @param imageDir
     */
    public Photo(String imageDir,String date,String description,ArrayList<Tag> Tags){
        this.imageDir = imageDir;
        this.date = date;
        this.description = description;
        this.Tags = Tags;
    }

    /**getter for image direcotry
     *
     * @return
     */
    public String getImageDir(){return imageDir;}
    /**getter for image date
     *
     * @return
     */
    public String getDate(){return date;}
    /**getter for image description
     *
     * @return
     */
    public String getDescription(){return description;}
    /**getter for image tags
     *
     * @return
     */
    public ArrayList<Tag> getTags(){return Tags;}


    /**setting the description
     *
     * @param description
     */
    public void setDescription(String description){this.description = description;
    }

    /**adding tags
     *
     * @param t
     */
    public boolean addTags(Tag t){
        for(Tag T : Tags){
            if(T.name.equals(t.name)){
                if(T.value.equals(t.value))
                    return false;
            }
        }
        Tags.add(t);
        return true;
    }

    /**removing a tag
     *
     * @param t
     * @return
     */
    public boolean removeTags(Tag t){
        for(Tag T : Tags){
            if(T.name.equals(t.name)){
                if(T.value.equals(t.value))
                    Tags.remove(T);
                    return true;
            }
        }
        return false;
    }
}
