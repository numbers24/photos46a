package objects;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**special type of user that has access to all accounts in userdata
 *
 */
public class admin extends user{

    /**admin constructor
     *
     * @param name
     * @param password
     */
    public admin(String name, String password) {
        super(name, password);
    }

    /**prints out the list of users
     *
     * @param users
     */
    public static void showUsers(ArrayList<user> users){
        for(user u : users) System.out.println(u.getName()+","+u.getPassword());
    }
    /**create a user under the admin powers
     *
     * @param username
     * @param password
     * @param users
     * @return
     */
    public static ArrayList<user> create(String username, String password, ArrayList<user> users) throws IOException {
        for(user u: users){
            if(u.name.equals(username)&&u.password.equals(password)) System.out.println("Err: account already exists"); //error Account Already Exists
        }
        new File("userdata/"+username).mkdir();
        new File("userdata/" + username +"/stock.txt").createNewFile();
        FileWriter f = new FileWriter(new File("userdata/" + username +"/albums.txt"));
        f.write("stock,userdata/stock/photos/justintgibbon.jpg");
        f.close();
        Album stock = new Album("stock","userdata/stock/photos/justintgibbon.jpg");
        stock.readAlbum("stock");
        stock.writeAlbum(username);

        users.add(new user(username,password));
        return users;
    }

    /**the idea here is that you can only remove a user that already exists in the UI
     *
     * @param username
     * @param password
     * @param users
     * @return
     */
    public static ArrayList<user> delete(String username,String password, ArrayList<user> users){
        if((username.equals("admin")&&password.equals("admin")) ||(username.equals("stock")&&password.equals("stock"))){
            System.out.println("Cannot delete this account");
            return users;
        }
        for(user u : users){
            if(u.name.equals(username)&&u.password.equals(password))
            {users.remove(u);break;}
        }
        new File("userdata/"+username).delete();
        return users;
    }
}
