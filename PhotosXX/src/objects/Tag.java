package objects;

import java.util.ArrayList;

/**object for tags
 *
 */
public class Tag {

    /**name of the tag
     *
     */
    String name;
    /**value pairs of the tag
     *
     */
    ArrayList<String> value;

    /**constructor for tags
     *
     * @param name
     * @param value
     */
    public Tag(String name, ArrayList<String> value){
        this.name=name;
        this.value=value;
    }

    /**get tag name
     *
     * @return
     */
    public String getName() {return name;}

    /**get tage value
     *
     * @return
     */
    public ArrayList<String> getValue(){return value;}

    /**adds another value to value list
     *
     * @param v
     */
    public boolean addValue(String v){
        for(String s : value){
            if(s.equals(v)) return false;
        }
        value.add(v);
        return true;
    }
}
