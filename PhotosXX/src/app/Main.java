package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Paths;

/** startup for the whole procedure
 *
 */
public class Main extends Application {

    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/app/Login.fxml"));
        AnchorPane root = (AnchorPane)loader.load();
        Login controller = loader.getController();
        controller.start(primaryStage);
        Scene s = new Scene (root, 350,400);
        primaryStage.setScene(s);
        primaryStage.show();
    }
    public static void main(String[] args){
        launch(args);
    }
}
