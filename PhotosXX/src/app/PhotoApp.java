package app;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.*;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

/**Runs the Photo Application
 *
 */
public class PhotoApp {
    /**holds the user data
     *
     */
    user user;
    /**holds the albums of the user
     *
     */
    ArrayList<Album> albums;
    /**holds the stage
     *
     */
    Stage s;
    /**holds directory files
     *
     */
    File f;
    /**holds the photo being copied or cutted
     *
     */
    Photo clipboard;

    public void start(Stage primaryStage, user user) throws Exception {
        this.user = user;
        albums = new ArrayList<Album>();
        readAlbums();
        primaryStage.setTitle("Image Album");
        s=primaryStage;
        f=null;
        filter="";

        if(user.getName().equals("admin")&&user.getPassword().equals("admin"))
            try{runAdmin();} catch(Exception e){System.out.println("Admin could not run properly");}
    }

    /**Searches Albums
     *
     */
    public void searchAlbums(){
        Stage primarystage = s;
        VBox albumlist = new VBox();
        for(Album a : albums){
            GridPane pane = new GridPane();

            Button accessAlbum = new Button(a.getName());
            accessAlbum.setOnAction(e ->{
                viewAlbum(a);
                try {
                    writeAlbums();
                } catch (Exception exception) {System.out.println("could not view album");}
            });

            Path p = Paths.get(a.getThumbnail());
            ImageView thumbnail = new ImageView("file:" + p.toAbsolutePath());
            thumbnail.setFitHeight(200);
            thumbnail.setFitWidth(200);

            Button delete = new Button("Delete");
            delete.setOnAction(e ->{
                albums.remove(a);
                new File("userdata/"+user.getName()+"/"+a.getName()+".txt").delete();
                try {
                    writeAlbums();
                    searchAlbums();
                } catch (Exception exception){}
            });

            HBox hbox = new HBox(accessAlbum,delete);
            pane.getChildren().add(thumbnail);
            pane.getChildren().add(hbox);
            pane.setAlignment(Pos.CENTER);
            albumlist.getChildren().add(pane);
        }
        albumlist.setAlignment(Pos.CENTER);
        albumlist.setSpacing(25);

        Button cancel = new Button("Cancel");
        cancel.setOnAction(e ->{
            try {
                new Login().launchApp(s,user);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        primarystage.setOnCloseRequest(e ->{
            try {
                writeAlbums();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        albumlist.getChildren().add(cancel);
        ScrollPane sp = new ScrollPane();
        sp.setContent(albumlist);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sp.setFitToWidth(true);
        primarystage.setScene(new Scene(sp,300,275));
    }

    /**
     * filter for searching
     */
    public String filter;
    /**views album
     *
     * @param a
     */
    public void viewAlbum(Album a){
        Stage primaryStage = s;
        ArrayList<Photo> album = a.getAlbum();

        TextField tf = new TextField();
        Button rename = new Button("Rename Album");
        rename.setOnAction(e ->{
            if (tf.getText().equals("")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(primaryStage);
                alert.setTitle("Image Album");
                alert.setHeaderText("Album Error");
                alert.setContentText("Please Enter a name to Rename");
                alert.showAndWait();
            }
            else {
                new File("userdata/"+user.getName()+"/"+a.getName()+".txt").delete();
                a.setName(tf.getText());
            }
        });
        Button search = new Button("Search");
        search.setOnAction(e ->{
            filter = tf.getText();
            viewAlbum(a);
        });

        Button addImg = new Button("Add Image");
        addImg.setOnAction(e ->{
            FileChooser fc = new FileChooser();
            fc.setTitle("Choose Image");
            f=fc.showOpenDialog(new Stage());
            if(f==null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(primaryStage);
                alert.setTitle("Image Album");
                alert.setHeaderText("Image Error");
                alert.setContentText("Please Choose an Image");
                alert.showAndWait();
            }
            else{
                SimpleDateFormat sdf = new SimpleDateFormat("E, MMMM dd HH:mm:ss z yyyy");
                a.addPhoto(new Photo(f.getAbsolutePath(),sdf.format(f.lastModified()),"",new ArrayList<Tag>()));
            }
            viewAlbum(a);
        });
        Button paste = new Button("Paste");
        paste.setOnAction(e ->{
            if(clipboard!=null)
                a.addPhoto(clipboard);
            viewAlbum(a);
        });

        HBox top = new HBox(rename,search);
        top.setAlignment(Pos.CENTER);
        top.setSpacing(20);

        HBox bot = new HBox(addImg,paste);
        bot.setAlignment(Pos.CENTER);
        bot.setSpacing(20);

        VBox vbox = new VBox(tf,top,bot);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);

        for(Photo p : album){
            if(!filter.equals("")){
                Boolean isFiltered=false;
                if(p.getDate().equals(filter)){isFiltered=true;}
                for(Tag t : p.getTags()){
                    if(t.getName().equals(filter)){isFiltered=true;}
                    for(String s : t.getValue())
                        if(s.equals(filter)){isFiltered=true;}
                }
                if(!isFiltered) continue;
            }
            Path pth = Paths.get(p.getImageDir());
            ImageView photo = new ImageView("file:"+pth.toAbsolutePath());
            photo.setFitHeight(200);
            photo.setFitWidth(200);

            Button view = new Button("View");
            view.setOnAction(e ->{
                try {
                    Stage secondaryStage = new Stage();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/app/View.fxml"));
                    AnchorPane root = (AnchorPane) loader.load();
                    View controller = loader.getController();
                    controller.start(secondaryStage, p, user);
                    Scene s = new Scene(root, 1000, 700);
                    secondaryStage.setScene(s);

                    viewAlbum(a);
                    writeAlbums();
                }catch (Exception exception){System.out.println("failed to view image");}
            });
            Button copy = new Button("Copy");
            copy.setOnAction(e ->{
                clipboard = p;
            });
            Button cut = new Button("Cut");
            cut.setOnAction(e ->{
                clipboard = p;
                album.remove(p);
                a.setAlbum(album);
                viewAlbum(a);
            });
            Button delete = new Button("Delete");
            delete.setOnAction(e ->{
                album.remove(p);
                a.setAlbum(album);
                try{writeAlbums();}catch(Exception e2){}
                viewAlbum(a);
            });

            GridPane pane = new GridPane();
            pane.getChildren().addAll(photo);
            pane.getChildren().add(new HBox(view,copy,cut,delete));

            pane.setAlignment(Pos.CENTER);
            vbox.getChildren().add(pane);
        }
        Button cancel = new Button("Cancel");
        cancel.setOnAction(e ->{
            searchAlbums();
        });
        primaryStage.setOnCloseRequest(e ->{
            try {
                writeAlbums();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });

        vbox.getChildren().add(cancel);
        ScrollPane sp = new ScrollPane();
        sp.setContent(vbox);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sp.setFitToWidth(true);
        primaryStage.setScene(new Scene(sp,300,275));

    }
    /**Creates new Albums
     *
     */
    public void createAlbum(){
        Stage primaryStage = s;
        Label l = new Label("Enter Album Name:");
        TextField textField = new TextField();
        Button browse = new Button("Thumbnail");
        browse.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            f = fileChooser.showOpenDialog(new Stage());
        });
        Button accept = new Button("Accept");
        accept.setOnAction(e ->  {
            if(f==null || textField.getText().equals("")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(primaryStage);
                alert.setTitle("Image Album");
                alert.setHeaderText("Album Error");
                alert.setContentText("Either the thumbnail or the album name is missing!");
                alert.showAndWait();
            }
            else {
                try {
                    if (!addAlbum(textField.getText(), f.getAbsolutePath())){
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initOwner(primaryStage);
                        alert.setTitle("Image Album");
                        alert.setHeaderText("Album Error");
                        alert.setContentText("That Album Already Exists");
                        alert.showAndWait();
                    }
                    else new Login().launchApp(s,user);
                } catch (Exception exception) { System.out.println("Album creation unsuccessful"); }
            }        });
        VBox vbox = new VBox(l,textField,browse,accept);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        Button cancel = new Button("Cancel");
        cancel.setOnAction(e1 ->  {
            try {
                new Login().launchApp(s,user);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        vbox.getChildren().add(cancel);
        primaryStage.setScene(new Scene(vbox, 300, 275));
    }

    /**Updates all the album metadata stored for user
     *
     * @throws Exception
     */
    public void writeAlbums() throws Exception{
        FileWriter w = new FileWriter("userdata/"+user.getName()+"/albums.txt");
        for(Album a : albums){
            w.write(a.getName()+","+a.getThumbnail()+"\n");
            a.writeAlbum(user.getName());
        }
        w.close();
    }

    /**reads all the album metadata stored for a user
     *
     * @throws Exception
     */
    public void readAlbums() throws Exception{
        albums = new ArrayList<Album>();
        Scanner s = new Scanner(new File("userdata/"+user.getName()+"/albums.txt"));
        while(s.hasNextLine()){
            String[] line = s.nextLine().split(",");
            Album a = new Album(line[0],line[1]);
            a.readAlbum(user.getName());
            albums.add(a);
        }
        s.close();
    }

    public boolean addAlbum(String name, String thumbnail) throws Exception {
        for(Album a : albums){
            if(a.getName().equals(name)) return false;
        }
        albums.add(new Album(name,thumbnail));
        writeAlbums();
        return true;
    }

    /**logs out of user
     *
     * @throws Exception
     */
    public void Logout() throws Exception {
        Stage primaryStage = s;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/app/Login.fxml"));
        AnchorPane root = (AnchorPane)loader.load();
        Login controller = loader.getController();
        controller.start(primaryStage);
        Scene s = new Scene (root, 350,400);
        primaryStage.setScene(s);
        primaryStage.show();
    }

    /**runs the admin sub program
     *
     * @throws Exception
     */
    public void runAdmin() throws Exception {
        System.out.println("Admin SubProgram");
        Scanner s = new Scanner(System.in);
        Login l = new Login();
        l.readUsers();
        ArrayList<user> users = l.getUsers();
        while(true){
            System.out.println("Type In Number for Command: \n 1: List users \n 2: Create a new user \n 3: Delete an existing user \n 4: Exit");
            switch (s.nextLine()){
                case "1":
                    admin.showUsers(users);
                    break;
                case "2":
                    System.out.println("Username:");
                    String username = s.nextLine();
                    System.out.println("Password:");
                    String password = s.nextLine();
                    users = admin.create(username,password,users);
                    System.out.println("Done");
                    break;
                case "3":
                    System.out.println("Username:");
                    String username2= s.nextLine();
                    System.out.println("Password:");
                    String password2 = s.nextLine();
                    users = admin.delete(username2,password2,users);
                    System.out.println("Done");
                    break;
                case "4": return;
            }
            l.setUsers(users);
            l.writeUsers();
        }
    }


}
