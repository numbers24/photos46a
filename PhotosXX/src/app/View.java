package app;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import objects.Photo;
import objects.Tag;
import objects.user;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**object that views photos
 *
 */
public class View {

    @FXML
    ImageView image;
    @FXML
    Label name;
    @FXML
    TextArea description;
    @FXML
    Label date;
    @FXML
    ListView<String> tags;
    @FXML
    TextField tagname;
    @FXML
    TextArea tagvalue;

    /**holds the main stage
     *
     */
    Stage s;
    /**holds the user
     *
     */
    user user;
    /**holds the photo metadata
     *
     */
    Photo p;

    /**starts the process
     *
     * @param primaryStage
     * @param p
     * @param user
     */
    /**holds the tags in Listview
     *
     */
    public ObservableList<String> T;

    /**starts up View Stage
     *
     * @param primaryStage
     * @param p
     * @param user
     */
    public void start(Stage primaryStage,Photo p,user user){
        s=primaryStage;
        this.user = user;
        this.p = p;
        Path pth = Paths.get(p.getImageDir());
        image.setImage(new Image("file:"+pth.toAbsolutePath()));

        name.setText(p.getImageDir());
        description.setText(p.getDescription());
        date.setText(p.getDate());

        T = FXCollections.observableArrayList();
        listTags();

        s.show();
    }

    /**edits caption
     *
     */
    public void editDescription(){
        p.setDescription(description.getText());
    }

    /**adds tags to photo
     *
     */
    public void addTags(){
        String name = tagname.getText();
        ArrayList<String> value = new ArrayList<>();
        Scanner scn = new Scanner(tagvalue.getText());
        while(scn.hasNextLine()){
            value.add(scn.nextLine());
        }
        if(name.equals("") || value == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(s);
            alert.setTitle("Image View");
            alert.setHeaderText("Tag Addition Error");
            alert.setContentText("Please add Entry for both Tag name and Tag Values");
            alert.showAndWait();
        }

        if(!p.addTags(new Tag(name,value))){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(s);
            alert.setTitle("Image View");
            alert.setHeaderText("Tag Addition Error");
            alert.setContentText("That Tag Already Exists");
            alert.showAndWait();
        }
        listTags();
    }

    /**removes a tag for the list
     *
     */
    public void removeTags(){
        String[] t = tags.getSelectionModel().getSelectedItem().split(",");
        p.removeTags(new Tag(t[0], new ArrayList<String>(Arrays.asList(Arrays.copyOfRange(t,1,t.length)))));
        listTags();
    }

    /**adds tags to the list
     *
     */
    public void listTags(){
        T.clear();
        for(Tag t : p.getTags()){
            String s = t.getName();
            for(String str : t.getValue())
                s+=","+str;
            T.add(s);
        }
        tags.setItems(T);
    }
}
