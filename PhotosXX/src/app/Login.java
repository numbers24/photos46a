package app;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import objects.Album;
import objects.user;

import javafx.event.ActionEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**Login stage for users
 *
 */
public class Login{

    @FXML TextField username;
    @FXML TextField password;
    @FXML Button login;
    @FXML Button signup;

    private Stage s;
    ArrayList<user> users;

    public void start(Stage primaryStage)throws Exception{
        s=primaryStage;
        users = new ArrayList<user>();
        readUsers();
        primaryStage.setTitle("Login");
    }

    /**Logs in a user that already exists in the Admin files
     *
     * @param e
     * @throws Exception
     */
    public void guiLogin(ActionEvent e) throws Exception {
        String name = username.getText();
        String pass = password.getText();
        if(name.equals("")||pass.equals("")){
            throwError(s,1); return;
        }
        if(searchUsers(name,pass)){
            launchApp(s,new user(name,pass));
        }
        else throwError(s,2);
    }

    /**Signs up a new user
     *
     * @param e
     * @throws Exception
     */
    public void guiSignUp(ActionEvent e) throws Exception {
        String name = username.getText();
        String pass = password.getText();
        if(name.equals("")||pass.equals("")){
            throwError(s,3); return;
        }
        if(!searchUsers(name,pass)){
            addUsers(name,pass);
            launchApp(s,new user(name,pass));
        }
        else throwError(s,4);//error user already exists
    }

    /**throws different errors depending on how you fail to sign up
     *
     * @param mainS
     * @param err
     */
    public void throwError(Stage mainS, int err){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(mainS);
        alert.setTitle("Login");
        switch(err){
            case 1:
                alert.setHeaderText("Login Error");
                alert.setContentText("Please fill in a username and password");
                break;
            case 2:
                alert.setHeaderText("Login Error");
                alert.setContentText("This user does not exist");
                break;
            case 3:
                alert.setHeaderText("Signup Error");
                alert.setContentText("Please fill in a username and password");
                break;
            case 4:
                alert.setHeaderText("Signup Error");
                alert.setContentText("This already exists");
                break;

        }
        alert.showAndWait();
    }

    /**returns true if the user exists and false if not
     *
     * @param name
     * @param password
     * @return
     */
    public boolean searchUsers(String name, String password){
        for(user u : users){
            if (u.getName().equals(name) && u.getPassword().equals(password)) return true;
        }
        return false;
    }

    /**when you create a new account, cannot contain ','
     *
     * @param name
     * @param password
     */
    public void addUsers(String name, String password) {
        try {
            users.add(new user(name, password));
            new File("userdata/" + name).mkdir();
            new File("userdata/" + name +"/stock.txt").createNewFile();
            FileWriter f = new FileWriter(new File("userdata/" + name +"/albums.txt"));
            f.write("stock,userdata/stock/photos/justintgibbon.jpg");
            f.close();
            Album stock = new Album("stock","userdata/stock/photos/justintgibbon.jpg");
            stock.readAlbum("stock");
            stock.writeAlbum(name);
            writeUsers();
        }catch(Exception e) {System.out.println("account creation failure");}
    }

    /**reading the user logins to be stored in adminstrator's files
     *
     * @throws IOException
     */
    public void readUsers() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("userdata/admin/users.txt"));
        String line;
        while((line = br.readLine())!=null){
            String [] user = line.split(",");
            users.add(new user(user[0],user[1]));
        }
        br.close();
    }

    /** writing the user logins to be stored in adminstrator's files
     *
     * @throws IOException
     */
    public void writeUsers() throws IOException {
        FileWriter w = new FileWriter("userdata/admin/users.txt");
        for (user u : users) w.write(u.getName() + "," + u.getPassword() +"\n");
        w.close();
    }

    /**launches the Photo App
     *
     * @param primarystage
     * @param user
     * @throws Exception
     */
    public void launchApp(Stage primarystage, user user) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/app/PhotoApp.fxml"));
        AnchorPane root = (AnchorPane)loader.load();
        PhotoApp controller = loader.getController();
        controller.start(primarystage,user);
        Scene s = new Scene (root, 600,400);
        primarystage.setScene(s);
    }

    /**returns the list of users for admin
     *
     * @return
     */
    public ArrayList<user> getUsers(){return users;}

    /**returns the list bock to login from admin
     *
     * @param users
     */
    public void setUsers(ArrayList<user> users){this.users = users;}

    /** login constructor for admin
     *
     */
    public Login(){
        users = new ArrayList<user>();
    }

}
